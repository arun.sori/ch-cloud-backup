package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"net/http/httputil"
	"os"
	"regexp"
	"slices"
	"strings"
	"time"
)

var (
	orgID     string
	keyID     string
	keySecret string
	dryRun    bool
	logger    *slog.Logger
)

func init() {
	flag.StringVar(&orgID, "org-id", "", "Organization ID")
	flag.StringVar(&keyID, "key-id", "", "Key ID")
	flag.StringVar(&keySecret, "key-secret", "", "Key Secret")
	flag.BoolVar(&dryRun, "dry-run", true, "Only show which backups will be restored")
	logger = slog.Default()
	flag.Parse()
	if dryRun {
		slog.SetLogLoggerLevel(slog.LevelDebug)
	}
}

type ServicesResult struct {
	Result []Service `json:"result"`
}

type IPAccessList struct {
	Source      string `json:"source"`
	Description string `json:"description"`
}
type Service struct {
	ID           string         `json:"id"`
	Name         string         `json:"name"`
	Provider     string         `json:"provider"`
	Region       string         `json:"region"`
	Tier         string         `json:"tier"`
	IdleScaling  bool           `json:"idleScaling"`
	IdleTimeout  int64          `json:"idleTimeout"`
	IpAccessList []IPAccessList `json:"ipAccessList"`
	BackupID     string         `json:"backupId"`
}

type BackupResult struct {
	Result []Backup `json:"result"`
}

type Backup struct {
	ID         string    `json:"id"`
	ServiceID  string    `json:"serviceId"`
	StartedAt  time.Time `json:"startedAt"`
	FinishedAt time.Time `json:"finishedAt"`
	Status     string    `json:"status"`
}

func bodyCloser(Body io.ReadCloser) {
	err := Body.Close()
	if err != nil {
		logger.Error("resp closing", slog.String("error", err.Error()))
	}
}
func main() {
	if orgID == "" || keyID == "" || keySecret == "" {
		logger.Error("missing orgID or keyID or keySecret")
		os.Exit(1)
	}

	basicAuthVal := base64.URLEncoding.EncodeToString([]byte(keyID + ":" + keySecret))
	httpClient := http.Client{
		Timeout: 30 * time.Second,
	}

	servicesRequest, err := http.NewRequest(
		"GET",
		"https://api.clickhouse.cloud/v1/organizations/"+orgID+"/services",
		nil,
	)
	if err != nil {
		logger.Error("services list create request", slog.String("error", err.Error()))
		os.Exit(1)
	}

	servicesRequest.Header.Set(
		"Authorization", "Basic "+basicAuthVal)

	resp, err := httpClient.Do(servicesRequest)
	if err != nil {
		logger.Error("services list request", slog.String("error", err.Error()))
		os.Exit(1)
	}
	defer bodyCloser(resp.Body)
	if resp.StatusCode != http.StatusOK {
		logger.Error("getting services", slog.String("services resp status code", resp.Status))
		os.Exit(1)
	}

	buffer := bytes.Buffer{}

	_, err = buffer.ReadFrom(resp.Body)
	if err != nil {
		logger.Error("services list", slog.String("error", err.Error()))
		os.Exit(1)
	}

	services := ServicesResult{}
	err = json.Unmarshal(buffer.Bytes(), &services)
	if err != nil {
		logger.Error("services resp decoding", slog.String("error", err.Error()))
		os.Exit(1)
	}

	buffer.Reset()

	prodServices := make([]Service, 0)
	for _, service := range services.Result {
		if service.Tier != "production" {
			logger.Info("skipping development service", slog.String("name", service.Name))
			continue
		}
		prodServices = append(prodServices, service)
	}
	logger.Debug("got prod services", slog.Any("services", prodServices))

	for _, service := range prodServices {
		logger.Info("processing service", slog.String("name", service.Name))
		latestBackup, err := getLatestBackup(&httpClient, basicAuthVal, service.ID)
		if err != nil {
			logger.Error("getLatestBackup : ", slog.String("error", err.Error()))
			os.Exit(1)
		}
		if latestBackup != nil {
			logger.Info("latest backup ID", slog.String("ID", latestBackup.ID))
			err := restoreFromBackup(&httpClient, basicAuthVal, service, latestBackup.ID)
			if err != nil {
				logger.Error("restoreFromBackup: ", slog.String("error", err.Error()))
				os.Exit(1)
			}
		}
		err = cleanOldRestoredServices(&httpClient, basicAuthVal, &service)
		if err != nil {
			logger.Error("cleanOldRestoredServices : ", slog.String("error", err.Error()))
			os.Exit(1)
		}
	}

	os.Exit(0)
}

func cleanOldRestoredServices(client *http.Client, basicAuthVal string, service *Service) error {
	if strings.Index(service.Name, "_restored_") == -1 || !service.IdleScaling {
		return nil
	}
	re, err := regexp.Compile(`.+_restored_(?P<timestamp>.+)`)
	if err != nil {
		return fmt.Errorf("re compilation: %w", err)
	}
	matches := re.FindStringSubmatch(service.Name)
	if matches == nil {
		return nil
	}
	index := re.SubexpIndex("timestamp")
	if index == -1 {
		return nil
	}
	timestamp, err := time.Parse(time.RFC3339, matches[index])
	if err != nil {
		return fmt.Errorf("parsing timestamp in service name :%w", err)
	}
	if time.Now().UTC().Sub(timestamp) <= time.Hour*24 {
		logger.Info("service is not older than 24 hours", slog.String("service-name", service.Name))
		return nil
	}
	// stop service
	req, err := http.NewRequest(
		"PATCH",
		"https://api.clickhouse.cloud/v1/organizations/"+orgID+"/services/"+service.ID+"/state",
		bytes.NewBuffer([]byte(`{ command: "stop"}`)),
	)
	if err != nil {
		return fmt.Errorf("service stop request: %w", err)
	}
	if dryRun {
		body, _ := httputil.DumpRequest(req, true)
		logger.Info("will stop old restored backup as:", slog.String("body", string(body)))
		return nil
	}
	req.Header.Set("Authorization", "Basic "+basicAuthVal)
	resp, err := client.Do(req)
	defer bodyCloser(resp.Body)
	if err != nil {
		return fmt.Errorf("service stop request: %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("service stop response expected 200 got: %d", resp.StatusCode)
	}

	// delete service
	delteReq, err := http.NewRequest(
		"DELETE",
		"https://api.clickhouse.cloud/v1/organizations/"+orgID+"/services/"+service.ID,
		nil,
	)
	if err != nil {
		return fmt.Errorf("service delete req: %w", err)
	}
	if dryRun {
		body, _ := httputil.DumpRequest(req, true)
		logger.Info("will delete old restored backup as:", slog.String("body", string(body)))
		return nil
	}
	delteReq.Header.Set("Authorization", "Basic "+basicAuthVal)
	deleteResp, err := client.Do(req)
	defer bodyCloser(deleteResp.Body)
	if err != nil {
		return fmt.Errorf("service delete request: %w", err)
	}
	if deleteResp.StatusCode != http.StatusOK {
		return fmt.Errorf("service delete response expected 200 got: %d", deleteResp.StatusCode)
	}

	return nil
}

func getLatestBackup(client *http.Client, basicAuthVal, serviceID string) (*Backup, error) {
	buffer := bytes.Buffer{}
	backupReq, err := http.NewRequest(
		"GET",
		"https://api.clickhouse.cloud/v1/organizations/"+orgID+"/services/"+serviceID+"/backups",
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("backup req creation :%w", err)
	}
	backupReq.Header.Set("Authorization", "Basic "+basicAuthVal)
	resp, err := client.Do(backupReq)
	if err != nil {
		return nil, fmt.Errorf("backup request: %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("backup resp status code expected 200 got : %s", resp.Status)
	}
	defer bodyCloser(resp.Body)
	_, err = buffer.ReadFrom(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("backup response read :%w", err)
	}
	result := BackupResult{}
	err = json.Unmarshal(buffer.Bytes(), &result)
	if err != nil {
		return nil, fmt.Errorf("backup result unmarshal: %w", err)
	}
	buffer.Reset()
	bodyCloser(resp.Body)
	// sort backups by their finished time
	slices.SortFunc(result.Result, func(a, b Backup) int {
		if a.FinishedAt.Before(b.FinishedAt) {
			return -1
		}
		return 0
	})
	logger.Debug("got backup", slog.Any("result", result))
	if len(result.Result) == 0 {
		logger.Info("no backups found")
		return nil, nil
	}

	latestBackup := &result.Result[len(result.Result)-1]
	if latestBackup.Status != "done" {
		logger.Info("backup isn't finished yet", slog.String("backupId", latestBackup.ID))
	}
	return latestBackup, nil
}

func restoreFromBackup(client *http.Client, basicAuthVal string, service Service, backupID string) error {
	logger.Info("restoring service", slog.String("name", service.Name))

	service.BackupID = backupID
	service.Name = service.Name + "_restored_" + time.Now().UTC().Format(time.RFC3339)
	service.IdleScaling = true
	service.IdleTimeout = 5
	body, err := json.Marshal(service)
	if err != nil {
		return fmt.Errorf("service object unmarshal: %w", err)
	}
	req, err := http.NewRequest(
		"POST",
		"https://api.clickhouse.cloud/v1/organizations/"+orgID+"/services",
		bytes.NewBuffer(body),
	)
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		return fmt.Errorf("service restore request: %w", err)
	}
	if dryRun {
		body, _ := httputil.DumpRequest(req, true)
		logger.Info("will restore backup as:", slog.String("body", string(body)))
		return nil
	}

	req.Header.Set("Authorization", "Basic "+basicAuthVal)
	resp, err := client.Do(req)
	defer bodyCloser(resp.Body)
	if err != nil {
		return fmt.Errorf("service restore request: %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("service restore response expected 200 got %d", resp.StatusCode)
	}
	return nil
}
