GO_TEST_FLAGS ?= -race -count=1 -v -timeout=10m
GOOS ?= $(shell go env GOOS)
GOARCH ?= $(shell go env GOARCH)

build:
	go build ./...

.PHONY: clean
clean:
	@find . -type f -name 'ch-cloud-bkcup' -delete

docker:
	docker build \
			--build-arg GO_BUILD_LDFLAGS \
			--build-arg BUILDPLATFORM \
			--build-arg GO_VERSION=1.22 \
			-f Dockerfile \
			-t $(DOCKER_IMAGE) \
				.