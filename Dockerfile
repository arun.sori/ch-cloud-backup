ARG GO_VERSION=1.22

FROM --platform=${BUILDPLATFORM} golang:${GO_VERSION} AS build
ENV CGO_ENABLED=0

ARG GO_BUILD_LDFLAG

WORKDIR /build

# Cache dependencies
COPY go.mod ./
RUN --mount=type=cache,target=/go/pkg go mod download -x

COPY main.go ./

ARG GO_BUILD_LDFLAGS
ARG TARGETARCH
ARG TARGETOS
# Call go build directly instead of make target because otherwise we have to
# also pull in Makefile.shared from the parent directory and use a wider docker
# build context. Using `bash -c` format to avoid quoting issues.
RUN --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg \
["/bin/bash", "-c", "GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -ldflags \"$GO_BUILD_LDFLAGS\" -o app ./..."]

FROM gcr.io/distroless/base-debian11:nonroot
COPY --from=build /build/app /

WORKDIR /
USER 65532:65532

ENTRYPOINT ["/app"]
